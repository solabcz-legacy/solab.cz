# [solab.cz](http://solab.cz)

Website based on Jekyll and hosted by GitHub pages


Docs
----

Run development server on port 4000 `sh serve.sh`

Build site to `sh build.sh` to directory `build`

Deploy to GitHub pages `sh deploy-github.sh`

